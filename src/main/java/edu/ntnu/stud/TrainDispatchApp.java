package edu.ntnu.stud;

import edu.ntnu.stud.commands.Command;
import edu.ntnu.stud.commands.DepartureCreatorCommand;
import edu.ntnu.stud.commands.ExitApplicationCommand;
import edu.ntnu.stud.commands.PrintDeparturesCommand;
import edu.ntnu.stud.commands.RemoveDepartureCommand;
import edu.ntnu.stud.commands.SearchDepartureCommand;
import edu.ntnu.stud.commands.SetDelayCommand;
import edu.ntnu.stud.commands.SetTrackCommand;
import edu.ntnu.stud.commands.UpdateClockCommand;
import edu.ntnu.stud.input.InputHandler;
import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.utils.Halt;
import edu.ntnu.stud.utils.Renderer;
import java.time.LocalTime;


/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {

  private static final Command[] commands = {
      new PrintDeparturesCommand(),
      new UpdateClockCommand(),
      new DepartureCreatorCommand(new java.util.Random()),
      new RemoveDepartureCommand(),
      new SetTrackCommand(),
      new SetDelayCommand(),
      new SearchDepartureCommand(),
      new ExitApplicationCommand()};
  private static DepartureTable table;
  private static InputHandler inputHandler;

  /**
   * The main method for the train dispatch application.
   * Consist of initializing the application and starting it.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    init();
    start();
  }


  /**
   * Initializes the application by creating instances and starting data.
   */
  private static void init() {

    // Check if assertions are enabled
    try {
      assert false; // Throws and continues if assertions are enabled
      Halt.pressEnterToContinue("This application must be run with the -ea flag. Exiting.\n");
      Renderer.clearConsole();
      System.exit(0);

    } catch (AssertionError ignored) {
      System.out.println("Initializing..."); // Continue if assertions are enabled
    }


    // Creates a new departure table and input handler
    table = new DepartureTable(LocalTime.of(0, 0));
    inputHandler = new InputHandler();

    // Creates a few pre-generated train departures
    TrainDeparture[] testData = {
        new TrainDeparture(
            LocalTime.of(0, 33), "FLY12", 1435, "Bergen", LocalTime.of(1, 23), 1),
        new TrainDeparture(
            LocalTime.of(16, 15), "GHF4", 8264, "Trondheim", LocalTime.of(0, 53), 5),
        new TrainDeparture(
            LocalTime.of(8, 58), "134GH", 1375, "Oslo", LocalTime.of(0, 0), 3),
        new TrainDeparture(
            LocalTime.of(15, 25), "GTCD", 9641, "Oslo", LocalTime.of(0, 0), -1),
        new TrainDeparture(
            LocalTime.of(18, 56), "MK34", 3477, "Oslo", LocalTime.of(0, 23), 2),
        new TrainDeparture(
            LocalTime.of(12, 10), "456AS", 1523, "Kristiansand", LocalTime.of(0, 0),
            2),
        new TrainDeparture(
            LocalTime.of(23, 30), "3474VD", 9785, "Ålesund", LocalTime.of(0, 0), 1),
        new TrainDeparture(
            LocalTime.of(0, 30), "FGVR45", 2365, "Selbu", LocalTime.of(0, 27), 3),
        new TrainDeparture(
            LocalTime.of(1, 15), "43F", 1735, "Bergen", LocalTime.of(0, 0), 2),
        new TrainDeparture(
            LocalTime.of(10, 25), "A2B1", 5824, "Trondheim", LocalTime.of(0, 17), 5),
        new TrainDeparture(
            LocalTime.of(3, 10), "D4", 1863, "Stavanger", LocalTime.of(0, 0), 4),
        new TrainDeparture(
            LocalTime.of(14, 15), "345GF", 9364, "Norges teknisk-naturvitenskapelige universitet",
            LocalTime.of(0, 0), -1),
        new TrainDeparture(
            LocalTime.of(22, 55), "ASDF", 3747, "Trondheim", LocalTime.of(0, 0), -1),
        new TrainDeparture(
            LocalTime.of(10, 6), "B2", 1528, "Kragerø", LocalTime.of(0, 0), -1)
    };

    // Adds the pre-generated train departures to the departure table
    for (TrainDeparture departure : testData) {
      table.addDeparture(departure);
    }
  }


  /**
   * Starts the application. This method has an infinite loop which runs the application.
   */
  private static void start() {
    System.out.println("Starting...");

    int choice;

    //noinspection InfiniteLoopStatement
    while (true) {

      // Update the departure table
      table.updateDepartureTable();

      // Render the departure table
      Renderer.renderMenu(commands);

      // Get the user's choice
      String regexAndPrompt = "[1-" + (commands.length) + "]";
      choice = Integer.parseInt(inputHandler.getInput(
          "Enter an option",
          regexAndPrompt,
          regexAndPrompt,
          false));

      // Run the command
      try {
        commands[choice - 1].run(table);

        // Catch any exceptions thrown by the command and print the error message
      } catch (Exception e) {
        Halt.pressEnterToContinue("An error occurred: " + e.getMessage());
      }
    }
  }
}
