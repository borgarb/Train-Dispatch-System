package edu.ntnu.stud.models;

import edu.ntnu.stud.exceptions.NoDepartureFoundException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.OptionalInt;
import java.util.stream.IntStream;

/**
 * DepartureTable.java Class for creating a departure table and modifying/adding departures to it.
 */
public class DepartureTable {
  // Object variables
  private final ArrayList<TrainDeparture> departureList = new ArrayList<>();
  private LocalTime currentTime;

  /**
   * Constructor for the DepartureTable class.
   *
   * @param currentTime the current time of the new departure table.
   */
  public DepartureTable(LocalTime currentTime) {
    setCurrentTime(currentTime);
  }

  /**
   * Method for checking if the adjusted time is before the tables current time.
   *
   * @param adjustedTime the adjusted time to be checked.
   * @return true if the adjusted time is before the current time, false otherwise.
   */
  public boolean isAdjustedTimeBeforeCurrentTime(
       LocalTime adjustedTime) {

    return adjustedTime.isBefore(currentTime);
  }

  /**
   * Method for getting the departure list.
   *
   * @return the departure list.
   */
  public ArrayList<TrainDeparture> getDepartureList() {
    return departureList;
  }

  /**
   * Method for getting the departure at a given index.
   *
   * @param index the index of the departure to get.
   * @return the departure at the given index.
   */
  public TrainDeparture getDepartureAt(int index) {
    return departureList.get(index);
  }

  /**
   * Method for getting the current departure table time.
   *
   * @return the current departure table time.
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /** Sets the new current time.
   *
   * @param time the new current time to be set.
   */
  public void setCurrentTime(LocalTime time) {
    assert time != null : "Time cannot be null";
    assert currentTime == null || time.isAfter(currentTime)
        : "\nERROR: The new time cannot be before or equal to the current time";
    currentTime = time;
  }

  /**
   * Sorts and removes departed trains.
   */
  public void updateDepartureTable() {
    removeDepartedTrains();
    sortDepartureTable();
  }

  // Removes all trains that have departed
  private void removeDepartedTrains() {
    departureList.removeIf(departure -> departure.getAdjustedTime().isBefore(currentTime));
  }

  // Sorts the departure table by adjusted time then destination if the adjusted times are equal
  private void sortDepartureTable() {
    departureList.sort(Comparator.comparing(TrainDeparture::getAdjustedTime)
        .thenComparing(TrainDeparture::getDestination));
  }

  /** Adds a departure to the departure table.
   *
   * @param departure the departure to be added to the departure table. Cannot be null.
   */
  public void addDeparture(TrainDeparture departure) {
    assert departure != null : "Departure cannot be null";
    departureList.add(departure);
  }

  /**
   * Method for removing a departure from the departure table.
   *
   * @param index the index of the departure to be removed.
   */
  public void removeDeparture(int index) {
    departureList.remove(index);
  }

  /**
   * Method for getting the index of a departure by train ID.
   *
   * @param trainId the train ID of the departure to get the index of..
   * @return the index of the departure.
   */
  public int getIndexOfTrainId(int trainId)
      throws NoDepartureFoundException {

    // 1. Stream an integer range with same size as table (stream of indices)
    // 2. Filter the stream by train ID matching the train ID of the departure at the index 'i'
    // 3. Get the first matching index
    OptionalInt index = IntStream.range(0, getDepartureList().size())
        .filter(i -> getDepartureAt(i).getTrainId() == trainId)
        .findFirst();

    // Throw an exception if the index is empty
    if (index.isEmpty()) {
      throw new NoDepartureFoundException("No departure with the train ID " + trainId + " exists.");
    }

    // Return the index
    return index.getAsInt();
  }
}
