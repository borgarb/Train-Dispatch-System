package edu.ntnu.stud.models;

import java.time.LocalTime;

/**
 * TrainDeparture class for the program.
 */
public class TrainDeparture {
  private final LocalTime scheduledTime;
  private final String line;
  private final Integer trainId;
  private final String destination;
  private LocalTime delay;
  private int track;

  /**
   * Constructor for the TrainDeparture class.
   *
   * @param scheduledTime the scheduled time of departure.
   * @param line          the line of the train.
   * @param trainId       the train identification.
   * @param destination   the destination of the train.
   * @param delay         the delay of the train.
   * @param track         the track of the train.
   */
  // Constructor
  public TrainDeparture(
      LocalTime scheduledTime,
      String line,
      Integer trainId,
      String destination,
      LocalTime delay,
      int track) {

    assert trainId >= 1000 && trainId <= 9999 : "\nTrain ID must be a positive integer"
        + "between 1000 and 9999";
    assert scheduledTime != null : "\nScheduled time cannot be null";
    assert line != null : "\nLine cannot be null";
    assert destination != null : "\nDestination cannot be null";
    assert !destination.isEmpty() : "\nDestination cannot be empty";
    assert !line.isEmpty() : "\nLine cannot be empty";

    this.scheduledTime = scheduledTime;
    this.line = line;
    this.destination = destination;
    this.trainId = trainId;

    setTrack(track);
    setDelay(delay);
  }

  /** Returns a bool indicating if the adjusted time is valid.
   *
   * @param scheduledTime the scheduled time of departure.
   * @param delay        the delay of the train.
   * @return true if the adjusted time is valid, false if not.
   */
  public static boolean isAdjustedTimePastMidnight(LocalTime scheduledTime, LocalTime delay) {
    LocalTime adjustedTime = scheduledTime
        .plusHours(delay.getHour())
        .plusMinutes(delay.getMinute());

    return !(adjustedTime.isAfter(scheduledTime) || adjustedTime.equals(scheduledTime));
  }

  /**
   * Method for getting the scheduled time of the train.
   *
   * @return the scheduled time of the train.
   */
  public LocalTime getScheduledTime() {
    return scheduledTime;
  }

  /**
   * Method for getting the track of the train.
   *
   * @return the track of the train.
   */
  public int getTrack() {
    return track;
  }

  /**
   * Method for setting the track of the train.
   *
   * @param track the track of the train.
   */
  public void setTrack(int track) {
    assert track > 0 || track == -1 : "\nERROR: Track must be a positive integer or -1";
    this.track = track;
  }

  /**
   * Method for getting the line of the departure.
   *
   * @return the line of the departure.
   */
  public String getLine() {
    return line;
  }

  /**
   * Method for getting the train ID of the departure.
   *
   * @return the train ID of the departure.
   */
  public Integer getTrainId() {
    return trainId;
  }

  /**
   * Method for getting the destination of the train.
   *
   * @return the destination of the train.
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Method for getting the delay of the train.
   *
   * @return the delay of the train.
   */
  public LocalTime getDelay() {
    return delay;
  }

  /** Sets the delay of the train.
   *
   * @param delay the delay of the train.
   */
  public void setDelay(LocalTime delay) {
    assert !isAdjustedTimePastMidnight(scheduledTime, delay)
        : "\nERROR: The scheduled time (" + scheduledTime + ") plus the delay (" + delay
        + ") is past midnight.";
    this.delay = delay;
  }

  /**
   * Method for calculating and returning the adjusted time of departure.
   *
   * @return the adjusted time of departure.
   */
  public LocalTime getAdjustedTime() {
    return scheduledTime.plusHours(delay.getHour()).plusMinutes(delay.getMinute());
  }

}
