package edu.ntnu.stud.commands;

import static edu.ntnu.stud.utils.Constants.FORMAT_TRACK;
import static edu.ntnu.stud.utils.Constants.REGEX_TRACK;
import static edu.ntnu.stud.utils.Constants.REGEX_TRAINID;

import edu.ntnu.stud.exceptions.NoDepartureFoundException;
import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Halt;

/**
 * Class for setting/changing the track of a departure.
 */
public class SetTrackCommand extends Command {

  /**
   * Constructor for the SetTrackCommand class.
   */
  public SetTrackCommand() {
    super("Change the track of a departure");
  }


  /**
   * Method for changing the track of a departure.
   *
   * @param table the departure table which contains the departure to be changed.
   */
  @Override
  public void run(DepartureTable table) throws NoDepartureFoundException {
    String trainIdString = inputHandler.getInput(
        "Enter the train ID for the departure you want to change the track of, "
            + "or leave blank to abort",
        "[1000-9999]",
        REGEX_TRAINID,
        true);

    // If the input is empty, abort
    if (trainIdString.isEmpty()) {
      Halt.pressEnterToContinue("Input was empty. Aborting.");
      return;
    }

    // Get the index of the departure to change the delay of
    int trainId = Integer.parseInt(trainIdString);
    int index = table.getIndexOfTrainId(trainId);

    // Get the new delay from the user
    String trackString = inputHandler.getInput(
        "Enter the new track for the departure "
            + "(blank for no track). "
            + (table.getDepartureAt(index).getTrack() != -1
            ? "Current track is " + table.getDepartureAt(index).getTrack()
            : "The departure currently has no track assigned to it"),
        FORMAT_TRACK,
        REGEX_TRACK,
        true);

    // If the input is empty, set the delay to none
    int track;
    if (trackString.isEmpty()) {
      track = -1;
      table.getDepartureAt(index).setTrack(track);
      Halt.pressEnterToContinue("The track has been removed from the departure.");
      return;
    }

    track = Integer.parseInt(trackString);

    if (track == table.getDepartureAt(index).getTrack()) {
      Halt.pressEnterToContinue("The track is already set to " + track + ".");
      return;
    } else {
      Halt.pressEnterToContinue("The track has been changed to " + track);
    }

    // Set the delay of the departure
    try {
      table.getDepartureAt(index).setTrack(track);
    } catch (AssertionError e) {
      System.out.println(e.getMessage());
      Halt.pressEnterToContinue();
    }
  }
}
