package edu.ntnu.stud.commands;

import static edu.ntnu.stud.utils.Constants.REGEX_24HR;
import static edu.ntnu.stud.utils.Constants.REGEX_TRAINID;

import edu.ntnu.stud.exceptions.InvalidDepartureException;
import edu.ntnu.stud.exceptions.NoDepartureFoundException;
import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Halt;
import java.time.LocalTime;

/**
 * Class for setting/changing the delay of a departure.
 */
public class SetDelayCommand extends Command {

  public SetDelayCommand() {
    super("Set a new delay for a departure");
  }

  /**
   * Method for changing the delay of a departure.
   *
   * @param table the departure table which contains the departure to be changed.
   */
  @Override
  public void run(DepartureTable table)
      throws InvalidDepartureException, NoDepartureFoundException {

    // Get the train ID of the departure to change the delay of
    String trainIdString = inputHandler.getInput(
        "Enter the train ID for the departure you want to change the delay of, "
            + "or leave blank to abort",
        "[1000-9999]",
        REGEX_TRAINID,
        true);

    // If the input is empty, abort
    if (trainIdString.isEmpty()) {
      Halt.pressEnterToContinue("Input was empty. Aborting.");
      return;
    }

    // Get the index of the departure to change the delay of
    int trainId = Integer.parseInt(trainIdString);
    int index = table.getIndexOfTrainId(trainId);

    // Get the new delay from the user
    String delayString = inputHandler.getInput(
        "Enter the new delay for the departure "
            + "(blank for no delay). Current delay is "
            + table.getDepartureAt(index).getDelay(),
        "[HH:MM]",
        REGEX_24HR,
        true);

    // If the input is empty, set the delay to none
    LocalTime delay;
    if (delayString.isEmpty()) {
      delay = LocalTime.of(0, 0);

      // If the input is not empty, parse the input
    } else {
      delay = LocalTime.parse(delayString);
    }

    // Calculate the new adjusted departure time
    LocalTime newAdjustedDepartureTime = table.getDepartureAt(index)
        .getScheduledTime()
        .plusHours(delay.getHour())
        .plusMinutes(delay.getMinute());

    // True if the new adjusted departure time is past midnight
    boolean isNewAdjustedTimePastMidnight = newAdjustedDepartureTime
        .isBefore(table.getDepartureAt(index).getScheduledTime());

    // True if the new adjusted departure time is before the current time
    boolean isNewAdjustedTimeBeforeCurrentTime = table.getDepartureAt(index).getScheduledTime()
        .plusHours(delay.getHour())
        .plusMinutes(delay.getMinute())
        .isBefore(table.getCurrentTime());

    // If the new adjusted departure time is after midnight, throw an exception
    if (isNewAdjustedTimePastMidnight) {
      throw new InvalidDepartureException("The scheduled time ("
          + table.getDepartureAt(index).getScheduledTime() + ") " + "plus the delay ("
          + delay + ") is past midnight.");
    }

    // If the new adjusted departure time is before the current time, throw an exception
    if (isNewAdjustedTimeBeforeCurrentTime) {
      throw new InvalidDepartureException("The scheduled time ("
          + table.getDepartureAt(index).getScheduledTime() + ") " + "plus the delay ("
          + delay + ") is before the current time (" + table.getCurrentTime() + ").");
    }

    // Set the delay of the departure
    try {
      table.getDepartureAt(index).setDelay(delay);
      if (delay.equals(LocalTime.of(0, 0))) {
        System.out.println("The delay of the departure with train ID " + trainId
            + " has been removed.");
      } else {
        System.out.println("The delay of the departure with train ID " + trainId
            + " has been set to " + delay);
      }
    } catch (AssertionError e) {
      System.out.println(e.getMessage());
      Halt.pressEnterToContinue();
    }
  }
}
