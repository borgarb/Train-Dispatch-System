package edu.ntnu.stud.commands;

import edu.ntnu.stud.input.InputHandler;
import edu.ntnu.stud.models.DepartureTable;

/**
 * Abstract class for commands that can be run on the departure table.
 */
public abstract class Command {
  public final InputHandler inputHandler;
  private final String name;

  /**
   * Constructor for the abstract Command class.
   *
   * @param name the name of the command. Acts as a description.
   */
  public Command(String name) {
    this.name = name;
    inputHandler = new InputHandler();
  }

  /**
   * Method for running the command on the departure table.
   *
   * @param table the departure table to run the command on.
   * @throws Exception if something goes wrong.
   */
  public abstract void run(DepartureTable table) throws Exception;

  public String getName() {
    return name;
  }
}
