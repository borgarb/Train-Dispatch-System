package edu.ntnu.stud.commands;

import static edu.ntnu.stud.utils.Constants.FORMAT_TRACK;
import static edu.ntnu.stud.utils.Constants.REGEX_24HR;
import static edu.ntnu.stud.utils.Constants.REGEX_DESTINATION;
import static edu.ntnu.stud.utils.Constants.REGEX_LINE;
import static edu.ntnu.stud.utils.Constants.REGEX_TRACK;
import static edu.ntnu.stud.utils.Constants.REGEX_TRAINID;
import static edu.ntnu.stud.utils.Constants.REGEX_YES_NO;

import edu.ntnu.stud.exceptions.InvalidDepartureException;
import edu.ntnu.stud.input.InputHandler;
import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.utils.Halt;
import edu.ntnu.stud.utils.Renderer;
import java.time.LocalTime;
import java.util.Random;

/**
 * Class for adding a departure.
 */
public class DepartureCreatorCommand extends Command {

  private final Random random;

  /**
   * Constructor for the DepartureCreatorCommand class.
   *
   * @param random the random number generator. Used for generating random train IDs.
   */
  public DepartureCreatorCommand(Random random) {
    super("Add a departure to the departure table");
    this.random = random;
  }


  /**
   * Command-method for creating a new departure.
   *
   * @param table the departure table to run the command on.
   * @throws InvalidDepartureException Exception to throw if something goes wrong.
   */
  @Override
  public void run(DepartureTable table) throws InvalidDepartureException {

    String line = getLine(inputHandler);
    int trainId = getTrainId(table, inputHandler);
    String destination = getDestination(inputHandler);
    int track = getTrack(inputHandler);
    LocalTime delay = getDelay(inputHandler);
    LocalTime scheduledTime = getScheduledTime(inputHandler);

    // Throw if the adjusted time is past midnight
    if (TrainDeparture.isAdjustedTimePastMidnight(scheduledTime, delay)) {
      throw new InvalidDepartureException("The scheduled time ("
          + scheduledTime + ") plus the delay ("
          + delay + ") is past midnight.");
    }

    // Calculate the adjusted time
    LocalTime adjustedTime = scheduledTime
        .plusHours(delay.getHour())
        .plusMinutes(delay.getMinute());

    // Throw if the adjusted time is before the current time
    if (table.isAdjustedTimeBeforeCurrentTime(adjustedTime)) {
      throw new InvalidDepartureException("The scheduled time ("
          + scheduledTime + ") plus the delay ("
          + delay + ") is before the current time (" + table.getCurrentTime() + ").");
    }

    // Create the new departure
    try {
      TrainDeparture departure = new TrainDeparture(
          scheduledTime,
          line,
          trainId,
          destination,
          delay,
          track);

      // Ask if the details are correct
      verifyDetails(table, inputHandler, departure);
    } catch (AssertionError e) {
      System.out.println(e.getMessage());
      Halt.pressEnterToContinue();
    }
  }


  // Method for auto-generating a train ID
  private int autogenerateTrainId(DepartureTable table) {
    int trainId;

    do {
      trainId = 1000 + random.nextInt(9000);
    } while (!isIdUnique(table, trainId));

    System.out.println("Autogenerated train ID: " + trainId);

    return trainId;
  }


  // Method for checking whether a given train ID is unique
  private boolean isIdUnique(DepartureTable table, int trainId) {
    return table.getDepartureList().stream()
        .filter(departure -> departure.getTrainId() == trainId).findAny().isEmpty();
  }


  // Method for getting a train ID from the user
  private int getTrainId(DepartureTable table,
                         InputHandler inputHandler) throws InvalidDepartureException {
    String trainIdString = inputHandler.getInput(
        "Enter a unique train ID for the departure, or leave blank to autogenerate",
        "[1000-9999]",
        REGEX_TRAINID,
        true);

    if (trainIdString.isEmpty()) {
      return autogenerateTrainId(table);

    } else if (isIdUnique(table, Integer.parseInt(trainIdString))) {
      return Integer.parseInt(trainIdString);

    } else {
      throw new InvalidDepartureException("Train ID '" + trainIdString + "' is already in use.");
    }
  }

  private String getLine(InputHandler inputHandler) {
    // Get the line for the new departure from the user
    return inputHandler.getInput(
        "Enter the line of the departure",
        "[Anything]",
        REGEX_LINE,
        false);
  }

  private String getDestination(InputHandler inputHandler) {
    // Get the destination of the new departure from the user
    return inputHandler.getInput(
        "Enter the destination of the departure",
        "[A-z]",
        REGEX_DESTINATION,
        false);
  }

  // Method for getting a track from the user
  private int getTrack(InputHandler inputHandler) {
    String trackString = inputHandler.getInput(
        "Enter the track of the departure, or leave blank if unknown",
        FORMAT_TRACK,
        REGEX_TRACK,
        true);

    // Return -1 if the track is unknown (blank response)
    if (trackString.isEmpty()) {
      return -1;
    } else {
      return Integer.parseInt(trackString);
    }
  }

  private LocalTime getDelay(InputHandler inputHandler) {
    // Get delay for the new departure from the user
    String delayString = inputHandler.getInput(
        "Enter the delay of the departure",
        "[HH:MM]",
        REGEX_24HR,
        false);

    return LocalTime.parse(delayString);
  }

  private LocalTime getScheduledTime(InputHandler inputHandler) {
    // Get the new departure time from the user
    String scheduledTimeString = inputHandler.getInput(
        "Enter the scheduled time of the departure",
        "[HH:MM]",
        REGEX_24HR,
        false);

    return LocalTime.parse(scheduledTimeString);
  }

  // Method for verifying the details of the new departure
  private void verifyDetails(DepartureTable table,
                             InputHandler inputHandler, TrainDeparture departure) {
    Renderer.renderDetails(departure); // Render the details of the new departure

    // Prompt the user to verify the details of the new departure
    String isCorrectAnswer = inputHandler.getInput(
        "Is this correct?",
        "[Y/n]",
        REGEX_YES_NO,
        true);

    // Add the new departure to the departure table if the user confirms
    if (isCorrectAnswer.isEmpty() || isCorrectAnswer.equalsIgnoreCase("Y")) {
      table.addDeparture(departure);
      Halt.pressEnterToContinue("The departure has been added.");

      // Abort if the user does not confirm
    } else if (isCorrectAnswer.equalsIgnoreCase("N")) {
      Halt.pressEnterToContinue("The departure will not be added. Aborting.");
    }
  }
}
