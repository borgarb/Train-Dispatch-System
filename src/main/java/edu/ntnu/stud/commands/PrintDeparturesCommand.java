package edu.ntnu.stud.commands;

import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Halt;
import edu.ntnu.stud.utils.Renderer;

/**
 * Class for printing departures.
 */
public class PrintDeparturesCommand extends Command {

  /**
   * Constructor for the PrintDeparturesCommand class.
   */
  public PrintDeparturesCommand() {
    super("Show departure table");
  }

  /**
   * Prints all departures as a departure table.
   *
   * @param table The departure table to show.
   */
  @Override
  public void run(DepartureTable table) {
    Renderer.renderDepartureTable(table);
    Halt.pressEnterToContinue();
  }
}
