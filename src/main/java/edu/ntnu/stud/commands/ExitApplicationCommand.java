package edu.ntnu.stud.commands;

import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Renderer;

/**
 * Command class for exiting the application.
 */
public class ExitApplicationCommand extends Command {

  /**
   * Constructor for the ExitApplicationCommand class.
   */
  public ExitApplicationCommand() {
    super("Exit the application");
  }

  /**
   * Exits the application.
   *
   * @param table Needed due to inheritance.
   */
  @Override
  public void run(DepartureTable table) {
    System.out.println("Exiting application...");
    Renderer.clearConsole();
    System.exit(0);
  }
}
