package edu.ntnu.stud.commands;

import static edu.ntnu.stud.utils.Constants.REGEX_24HR;

import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Halt;
import java.time.LocalTime;

/**
 * Class for updating the clock of the system.
 */
public class UpdateClockCommand extends Command {

  /**
   * Constructor for the UpdateClockCommand class.
   */
  public UpdateClockCommand() {
    super("Update the clock of the system");
  }

  /**
   * Updates the clock of the system.
   *
   * @param table The table to change the current time of.
   */
  @Override
  public void run(DepartureTable table) {
    // Get the new time from the user
    String input = inputHandler.getInput("Enter new time, or leave blank to abort",
        "HH:MM",
        REGEX_24HR,
        true);

    // If the input is empty, abort
    if (input.isEmpty()) {
      Halt.pressEnterToContinue("\nThe time has not been changed.");

      // If the input is not empty, parse the input and set the new time
    } else {
      LocalTime newTime = LocalTime.parse(input);

      try {
        table.setCurrentTime(newTime);
        Halt.pressEnterToContinue("The time has been changed to " + newTime);
      } catch (AssertionError e) {
        System.out.println(e.getMessage());
        Halt.pressEnterToContinue();
      }
    }
  }
}
