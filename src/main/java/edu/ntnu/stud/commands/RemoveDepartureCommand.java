package edu.ntnu.stud.commands;

import static edu.ntnu.stud.utils.Constants.REGEX_TRAINID;

import edu.ntnu.stud.exceptions.NoDepartureFoundException;
import edu.ntnu.stud.models.DepartureTable;
import edu.ntnu.stud.utils.Halt;

/**
 * Class for removing a departure.
 */
public class RemoveDepartureCommand extends Command {

  /**
   * Constructor for the RemoveDepartureCommand class.
   */
  public RemoveDepartureCommand() {
    super("Remove a departure from the departure table");
  }

  /**
   * Method for removing a departure.
   *
   * @param table the departure table to remove a departure from.
   */
  @Override
  public void run(DepartureTable table) throws NoDepartureFoundException {
    // Get the train ID of the departure to remove from the table
    String trainIdString = inputHandler.getInput(
        "Enter the train ID for the departure you want to remove, or leave blank to abort",
        "[1000-9999]",
        REGEX_TRAINID,
        true);

    // If the input is empty, abort
    if (trainIdString.isEmpty()) {
      System.out.println("\nInput was empty. Aborting.");
      return;
    }

    int trainId = Integer.parseInt(trainIdString);

    int removeIndex = table.getIndexOfTrainId(trainId);

    // Remove the departure from the table.
    table.removeDeparture(removeIndex);
    Halt.pressEnterToContinue("Departure with the train ID " + trainId + " has been removed.");
  }
}
