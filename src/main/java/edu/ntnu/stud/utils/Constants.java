package edu.ntnu.stud.utils;

/**
 * Class for storing constants.
 */
public class Constants {
  public static final String REGEX_24HR = "^([01][0-9]|2[0-3]):[0-5][0-9]$";
  public static final String REGEX_TRAINID = "^[1-9]\\d{3}$";
  public static final String REGEX_ANYTHING = ".*";
  public static final String REGEX_LINE = "^.+$";
  public static final String REGEX_DESTINATION = "^[A-Za-zæøåÆØÅ\\s'-]+$";
  public static final int MAX_TRACKS = 5;
  public static final String REGEX_TRACK = "^[1-" + MAX_TRACKS + "]$";
  public static final String FORMAT_TRACK = "[1-" + MAX_TRACKS + "]";
  public static final String REGEX_YES_NO = "[ynYN\\n]";
  public static final String INVALID_INPUT_MESSAGE = "\nInvalid input. Please try again.";
}
