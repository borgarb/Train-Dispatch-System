package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.exceptions.NoDepartureFoundException;
import java.time.LocalTime;
import java.util.ArrayList;

class DepartureTableTest {

  private final TrainDeparture departure1 = new TrainDeparture(
      LocalTime.of(14, 0),
      "A1",
      1234,
      "Oslo",
      LocalTime.of(0, 15),
      1);
  private final TrainDeparture departure2 = new TrainDeparture(
      LocalTime.of(13, 0),
      "B2",
      4321,
      "Trondheim",
      LocalTime.of(0, 0),
      -1);
  private DepartureTable table;

  @BeforeEach
  public void setUp() {
    table = new DepartureTable(LocalTime.of(12, 30));
    table.addDeparture(departure1);
    table.addDeparture(departure2);
  }

  @Test
  void getDepartureList() {
    ArrayList<TrainDeparture> expected = new ArrayList<>();
    expected.add(departure1);
    expected.add(departure2);

    ArrayList<TrainDeparture> actual = table.getDepartureList();

    assertEquals(expected, actual, "Departure list should be equal");
  }

  @Test
  void getDepartureAt() {
    TrainDeparture actual = table.getDepartureAt(0);

    assertEquals(departure1, actual, "Departures should be equal");

    actual = table.getDepartureAt(1);

    assertEquals(departure2, actual, "Departures should be equal");
  }

  @Test
  void getCurrentTime() {
    LocalTime expected = LocalTime.of(12, 30);
    LocalTime actual = table.getCurrentTime();

    assertEquals(expected, actual, "Current time should be 12:30");
  }

  @Test
  @DisplayName("setCurrentTime() - Positive tests")
  void setCurrentTimePositive() {
    assertEquals(LocalTime.of(12, 30), table.getCurrentTime(), "Current time should be 12:30 before setting");

    table.setCurrentTime(LocalTime.of(13, 0));

    LocalTime expected = LocalTime.of(13, 0);
    LocalTime actual = table.getCurrentTime();

    assertEquals(expected, actual, "Current time should be 13:00 after setting");
  }

  @Test
  @DisplayName("setCurrentTime() - Negative tests")
  void setCurrentTimeNegative() {
    assertThrows(AssertionError.class, () -> table.setCurrentTime(null)); // Null
    assertThrows(AssertionError.class, () -> table.setCurrentTime(LocalTime.of(12, 0))); // Before current time
  }

  @Test
  void updateDepartureTable() {
    boolean isSorted = table.getDepartureAt(0).getAdjustedTime()
        .isBefore(table.getDepartureAt(1).getAdjustedTime());

    assertFalse(isSorted, "Departure at index 0 should not be before departure at index 1 before sorting");

    table.updateDepartureTable();

    isSorted = table.getDepartureAt(0).getAdjustedTime()
        .isBefore(table.getDepartureAt(1).getAdjustedTime());

    assertTrue(isSorted, "Departure at index 0 should be before departure at index 1 after sorting");

    table.setCurrentTime(LocalTime.of(14, 0));

    table.updateDepartureTable();

    assertEquals(1, table.getDepartureList().size(), "Departure list should have size 1 because one train has departed");
  }

  @Test
  @DisplayName("addDeparture() - Positive tests")
  void addDeparturePositive() {
    TrainDeparture departure3 = new TrainDeparture(
        LocalTime.of(15, 0),
        "C3",
        5678,
        "Bergen",
        LocalTime.of(0, 0),
        2);

    table.addDeparture(departure3);

    TrainDeparture actual = table.getDepartureAt(2);

    assertEquals(departure3, actual, "Departures should be equal");
  }

  @Test
  @DisplayName("addDeparture() - Negative tests")
  void addDepartureNegative() {
    assertThrows(AssertionError.class, () -> table.addDeparture(null)); // Null
  }

  @Test
  void removeDeparture() {
    table.removeDeparture(0);

    TrainDeparture actual = table.getDepartureAt(0);

    assertEquals(departure2, actual, "Departures should be equal");
  }

  @Test
  @DisplayName("getIndexOfTrainId() - Positive tests")
  void getIndexOfTrainIdPositive() {
    try {
      int expected = 0;
      int actual = table.getIndexOfTrainId(1234);

      assertEquals(expected, actual, "Index should be 0");

      expected = 1;
      actual = table.getIndexOfTrainId(4321);

      assertEquals(expected, actual, "Index should be 1");
    } catch (NoDepartureFoundException e) {
      e.getMessage();
    }
  }

  @Test
  @DisplayName("getIndexOfTrainId() - Negative tests")
  void getIndexOfTrainIdNegative() {
    // None of the following train IDs exist in the departure table, so it should throw an exception
    assertThrows(NoDepartureFoundException.class, () -> table.getIndexOfTrainId(5678));
    assertThrows(NoDepartureFoundException.class, () -> table.getIndexOfTrainId(0));
    assertThrows(NoDepartureFoundException.class, () -> table.getIndexOfTrainId(-1));
  }
}