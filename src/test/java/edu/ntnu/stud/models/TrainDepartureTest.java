package edu.ntnu.stud.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalTime;

class TrainDepartureTest {

  private TrainDeparture departure;

  @BeforeEach
  public void setUp() {
    departure = new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        1234,
        "Oslo",
        LocalTime.of(0, 15),
        1);
  }

  @Test
  @DisplayName("getScheduledTime() - Gets the scheduled time")
  void getScheduledTime() {
    LocalTime expected = LocalTime.of(12, 30);
    LocalTime actual = departure.getScheduledTime();

    assertEquals(expected, actual, "Scheduled time should be 12:30");
  }

  @Test
  @DisplayName("getTrack() - Gets the track")
  void getTrack() {
    int expected = 1;
    int actual = departure.getTrack();

    assertEquals(expected, actual, "Track should be 1");
  }

  @Test
  @DisplayName("setTrack() - Positive tests")
  void setTrackPositive() {
    departure.setTrack(2);

    int expected = 2;
    int actual = departure.getTrack();

    assertEquals(expected, actual, "Track should be 2");

    departure.setTrack(-1);

    expected = -1;
    actual = departure.getTrack();

    assertEquals(expected, actual, "Track should be -1");
  }

  @Test
  @DisplayName("setTrack() - Negative tests")
  void setTrackNegative() {
    assertThrows(AssertionError.class, () -> departure.setTrack(0));
    assertThrows(AssertionError.class, () -> departure.setTrack(-2));
    assertThrows(AssertionError.class, () -> departure.setTrack(-100));
  }

  @Test
  @DisplayName("getLine() - Gets the line")
  void getLine() {
    String expected = "A1";
    String actual = departure.getLine();

    assertEquals(expected, actual, "Line should be A1");
  }

  @Test
  @DisplayName("getTrainId() - Gets the train ID")
  void getTrainId() {
    int expected = 1234;
    int actual = departure.getTrainId();

    assertEquals(expected, actual, "Train ID should be 1234");
  }

  @Test
  @DisplayName("getDestination() - Gets the destination")
  void getDestination() {
    String expected = "Oslo";
    String actual = departure.getDestination();

    assertEquals(expected, actual, "Destination should be Oslo");
  }

  @Test
  @DisplayName("getDelay() - Gets the delay")
  void getDelay() {
    LocalTime expected = LocalTime.of(0, 15);
    LocalTime actual = departure.getDelay();

    assertEquals(expected, actual, "Delay should be 00:15");
  }

  @Test
  @DisplayName("setDelay() - Positive Tests")
  void setDelayPositive() {
    LocalTime expected = LocalTime.of(0, 30);
    departure.setDelay(LocalTime.of(0, 30));
    LocalTime actual = departure.getDelay();

    assertEquals(expected, actual, "Delay should be 00:30");
  }

  @Test
  @DisplayName("setDelay() - Negative Tests")
  void setDelayNegative() {
    assertThrows(AssertionError.class, () -> departure.setDelay(LocalTime.of(23, 59)));
  }

  @Test
  @DisplayName("getAdjustedTime() - Gets the adjusted time")
  void getAdjustedTime() {
    LocalTime expected = LocalTime.of(12, 45);
    LocalTime actual = departure.getAdjustedTime();

    assertEquals(expected, actual, "Adjusted time should be 12:45");
  }

  @Test
  @DisplayName("Constructor - Positive Tests")
  void constructorPositive() {
    assertDoesNotThrow(() -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        1234,
        "Oslo",
        LocalTime.of(0, 15),
        -1));

    assertDoesNotThrow(() -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        7589,
        "Oslo",
        LocalTime.of(0, 15),
        1));
  }

  @Test
  @DisplayName("Constructor - Negative Tests")
  void constructorNegative() {
    // Invalid TrainID
    assertThrows(AssertionError.class, () -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        123,
        "Oslo",
        LocalTime.of(0, 15),
        -1));

    // Invalid TrainID
    assertThrows(AssertionError.class, () -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        12345,
        "Oslo",
        LocalTime.of(0, 15),
        -1));

    // Invalid track
    assertThrows(AssertionError.class, () -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        1234,
        "Oslo",
        LocalTime.of(0, 15),
        0));

    // Invalid track
    assertThrows(AssertionError.class, () -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        1234,
        "Oslo",
        LocalTime.of(0, 15),
        -2));

    // Invalid track
    assertThrows(AssertionError.class, () -> new TrainDeparture(
        LocalTime.of(12, 30),
        "A1",
        1234,
        "Oslo",
        LocalTime.of(0, 15),
        -100));
  }
}
