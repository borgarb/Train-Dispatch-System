# Portfolio project IDATA1003 - 2023

STUDENT NAME = "Borgar Barland"  
STUDENT ID = "111723"

## Project description

This is solution to a semester-ending project given at NTNU in the subject IDATT1003 (Programming 1).

It is entirely written in Java, and represents a train dispatch system for a singular train station. With this
application you can manage train departures and add/remove them as you please. The GUI is entirely text-based.

Instructions on how to run the application is given further down below.

## Project structure

This project utilizes the following folder structure:

```bash
├── README.md
└── src
    ├── main/java/edu/ntnu/stud         (sourcecode for application)
    │   ├── TrainDispatchApp.java       (Main-class)
    │   ├── commands
    │   │   └── (all command classes)
    │   ├── exceptions
    │   │   └── (custom exceptions)
    │   ├── input
    │   │   └── (input classes)
    │   ├── models
    │   │   └── (data models)
    │   ├── utils
    │   │   └── (utility classes)
    │   └── commands
    │       └── (all command classes)
    │
    └── test/java/edu/ntnu/stud         (sourcecode for tests)
        └── models
            └── (classes for testing data models)
```

All sourcecode for the application itself is located in the src/main/java/edu/ntnu/stud folder, and all tests are located in the src/test/java/edu/ntnu/stud folder.

The application is using a folder structure categorizing the classes into packages. The packages are as follows:
- commands: contains all command classes, which are used to execute commands given by the user.
- exceptions: contains all custom exceptions.
- input: contains all classes used for input handling.
- models: contains all data models.
- utils: contains all utility classes.

The Main-method is located at src/main/java/edu/ntnu/stud/TrainDispatchApp.java. This method is the entry point of the application.

This way we can easily find the classes we are looking for, and it makes the code more readable and developing-friendly as it minimizes coupling.


## Link to repository

The remote repository can be found on GitLab by clicking [here](https://gitlab.stud.idi.ntnu.no/borgarb/Train-Dispatch-System):

## How to run the project

WARNING: You need to have Java 16 or later installed to run this application.
Download Java 21 for [Linux](https://www.oracle.com/java/technologies/downloads/#jdk21-linux), [Windows](https://www.oracle.com/java/technologies/downloads/#jdk21-windows), or [macOS](https://www.oracle.com/java/technologies/downloads/#jdk21-mac).

You can either run the pre-compiled file TrainDispatchApp.jar in the 'bin'-folder, or compile from source yourself. It is recommended to run the application in an external terminal (not IDE terminals). To run the pre-compiled
application on Windows, double-click the windows.bat file. For Linux and macOS follow these steps:

1. Open a terminal and navigate to the bin-folder.
2. Run the command `java -ea -jar TrainDispatchSystem.jar`

You can also compile the application from source yourself, using the 'javac' and 'jar' commands or by using any IDE of your choice.
To compile and run from source using Intellij IDEA or Visual Studio Code, simply

1. open the train-dispatch-system folder in the IDE of your choice.
2. Navigate to the Main-class (which is TrainDispatchApp.java file, located at src/main/java/edu/ntnu/stud).
3. Open it and hit the arrow in the top-right corner. Make sure you run it with the -ea flag. A console should open at the bottom with the application running (WARNING: running in an IDE might break the console-clearing functionality).

## How to run the tests

The tests are located at src/test/java/edu/ntnu/stud

To run the tests, open the project using any IDE with JUnit 5 installed and run the tests for the methods you'd like test.

## References

Code for getting operating system name was written by '[C. K. Young](https://stackoverflow.com/users/13/c-k-young)' on [stackoverflow](https://stackoverflow.com/a/228481).

Code for clearing the console on Linux and macOS was written by '[Bhuvanesh Waran](https://stackoverflow.com/users/3908580/bhuvanesh-waran)' on [stackoverflow](https://stackoverflow.com/a/40041221). 

Code for clearing the console on Windows was written by '[Holger](https://stackoverflow.com/users/2711488/holger)' on [stackoverflow](https://stackoverflow.com/a/33379766).
